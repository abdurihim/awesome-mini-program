## 微信小程序资源大全 -- 持续更新中

- 文档类
  - <a href="https://mp.weixin.qq.com/debug/wxadoc/dev/index.html" target="_blank">小程序官方文档</a>
  - <a href="https://developers.weixin.qq.com/home" target="_blank">微信开发者社区</a>
  - <a href="https://github.com/justjavac/awesome-wechat-weapp" target="_blank">微信小程序开发资源汇总</a>
  - <a href="https://github.com/liujians/Wa-UI" target="_blank">针对微信小程序整合的一套UI库</a>

- 论坛社区类
  - <a href="http://www.henkuai.com/forum.php" target="_blank">很快微信开发者平台</a>
  - <a href="http://wxopen.club/" target="_blank">WXOPEN Club | 微信小程序专业社区</a>
  - <a href="http://weixin1024.cn/portal.php" target="_blank">蜂鸟小程序开发者社区</a>
  - <a href="http://www.wxapp-union.com/" target="_blank">小程序社区</a>
  - <a href="https://www.jianshu.com/c/61ded4b9708f" target="_blank">简书-小程序话题</a>
  - <a href="https://www.zhihu.com/topic/20061410/hot" target="_blank">知乎-小程序话题</a>

- UI框架
  - <a href="https://www.color-ui.com/" target="_blank">ColorUI 鲜亮的高饱和色彩，专注视觉的小程序组件库 </a>
  - <a href="https://meili.github.io/min/docs/minui/" target="_blank">MinUI</a>
  - <a href="https://github.com/phonycode/wuss-weapp" target="_blank">wuss-weapp</a> wuss-weapp 一款高质量，组件齐全，高自定义的微信小程序UI组件库
  - <a href="https://github.com/meili/minui" target="_blank">基于规范的小程序 UI 组件库，自定义标签组件，简洁、易用、工具化</a>

- 工具类
  - <a href="https://github.com/treadpit/wx_calendar" target="_blank">wx_calendar 微信小程序－日历 📅</a>
  - <a href="https://github.com/jonnyshao/wefetch" target="_blank">jonnyshao/wefetch 小程序请求扩展</a>
  - <a href="https://github.com/bigmeow/minapp-api-promise" target="_blank">微信小程序所有API promise化，支持await、支持请求列队</a>
  - <a href="https://github.com/tencentyun/wecos" target="_blank">WeCOS</a> WeCOS——微信小程序 COS 瘦身解决方案
  - <a href="https://github.com/icindy/wxParse" target="_blank">wxParse</a> wxParse-微信小程序富文本解析自定义组件，支持HTML及markdown解析
  - <a href="https://github.com/icindy/WxNotificationCenter" target="_blank">WxNotificationCenter</a> 微信小程序通知广播模式类,降低小程序之间的耦合度
  - <a href="https://github.com/icindy/wxSearch" target="_blank">wxSearch</a> 微信小程序优雅的搜索框
  - <a href="https://github.com/kiinlam/wetoast" target="_blank">wetoast</a> 微信小程序toast增强插件 
  - <a href="https://github.com/zhongjie-chen/wx-drawer" target="_blank">wx-drawer</a> 小程序模仿QQ6.0侧滑菜单
  - <a href="https://github.com/zhongjie-chen/wx-scrollable-tab-view" target="_blank">wx-scrollable-tab-view</a> 小程序可滑动得tab-view
  - <a href="https://github.com/o2team/wxapp-img-loader" target="_blank">wxapp-img-loader</a> 适用于微信小程序的图片预加载组件
  - <a href="https://github.com/o2team/wxapp-market" target="_blank">wxapp-market</a> 小程序营销组件，Marketing components for WeChatApp
  - <a href="https://github.com/DowneyL/discuz-wechat-miniprogram" target="_blank">discuz-wechat-miniprogram</a> Discuz 论坛实现接口，以及结合 Discuz 的微信小程序
  - <a href="https://github.com/Jeff2Ma/WeApp-Workflow" target="_blank">discuz-wechat-miniprogram</a> 基于Gulp 的微信小程序前端开发工作流
  - <a href="https://github.com/Tencent/wepy" target="_blank">WePY 小程序组件化开发框架</a>
  - <a href="https://github.com/chemzqm/wept" target="_blank">微信小程序 web 端实时运行工具 </a>
  - <a href="https://github.com/sqrtqiezi/mini-music-player" target="_blank">基于小程序开发的仿百度云音乐 demo</a>
  - <a href="https://github.com/cytle/wechat_web_devtools" target="_blank">wechat_web_devtools 微信开发者工具(微信小程序)linux完美支持</a>
  - <a href="https://github.com/nanwangjkl/sliding_puzzle" target="_blank">微信小程序，一个滑块拼图游戏</a>
  - <a href="https://github.com/natee/wxapp-2048" target="_blank">微信小程序2048</a>
  - <a href="https://github.com/kraaas/timer" target="_blank">番茄时钟微信小程序版</a>
  - <a href="https://github.com/youzan/zanui-weapp" target="_blank">高颜值、好用、易扩展的微信小程序 UI 库，Powered by 有赞</a>
  - <a href="https://github.com/yingye/weapp-qrcode" target="_blank">weapp.qrcode.js 在 微信小程序 中，快速生成二维码</a>
  - <a href="https://gitee.com/sansanC/wechatApp" target="_blank">微信小程序电商源码</a>
  - <a href="https://gitee.com/mirrors/wechat-app-mall" target="_blank">微信小程序商城，微信小程序微店</a>
  - <a href="https://gitee.com/dotton/lendoo-wx" target="_blank">灵动电商开源系统之微信小程序端</a>
  - <a href="https://github.com/lin-xin/wxapp-mall" target="_blank">微信小程序 商城demo</a>
  - <a href="https://github.com/80percent/wechat-weapp-mobx" target="_blank">微信小程序(wechat weapp) mobx 绑定, 页面间通信的利器</a>
  - <a href="https://github.com/qiu8310/minapp" target="_blank">重新定义微信小程序的开发 </a>
  - <a href="https://github.com/tvfe/txv-miniprogram-plugin" target="_blank">这是腾讯视频小程序播放插件。 打广告：腾讯视频小程序开发框架 </a>
  - <a href="https://github.com/tvfe/wxpage" target="_blank">WXPage 是一个极其轻量的微信小程序开发框架</a>
  - <a href="https://github.com/baqihg/wxTimer" target="_blank">微信小程序中的定时器（用于倒计时）</a>
  - <a href="https://github.com/ecitlm/wechat-app" target="_blank">微信小程序开发骨架、以及网络请求封装</a>
  - <a href="https://github.com/TooBug/wemark" target="_blank">wemark</a> 微信小程序Markdown渲染库
  - <a href="https://github.com/chemzqm/wept" target="_blank">wept</a> 微信小程序 web 端实时运行工具
- 源代码类
  - <a href="https://github.com/RebeccaHanjw/weapp-wechat-zhihu" target="_blank">weapp-wechat-zhihu</a> 微信中的知乎--微信小程序 demo // Zhihu in Wechat
  - <a href="https://github.com/gxt19940130/demos" target="_blank">wechat-zhihu-app</a>zhihu app
  - <a href="https://github.com/AndIMissU/Stars" target="_blank">Stars</a> 仿知识星球App（微信小程序）
  - <a href="https://github.com/tzc123/wx_project_meituan" target="_blank">wx_meituan</a> 微信小程序（仿美团外卖）
  - <a href="https://github.com/Mynameisfwk/vivo-shop" target="_blank">vivo-shop</a> 基于vue2.0实现的vivo移动端商城(vue+vue-ruoter+vue-resource+webpack)
  - <a href="https://github.com/Mynameisfwk/wechat-app-vivo" target="_blank">wechat-app-vivo</a> 微信小程序高仿vivo商城
  - <a href="https://github.com/ecitlm/wx-nba" target="_blank">wx-nba</a> 小程序实现的一个NBA球赛直播应用
  - <a href="https://github.com/julytian/qqMusicPlayer" target="_blank">微信小程序版QQ音乐</a>
  - <a href="https://github.com/lingxiaoyi/navigation-bar" target="_blank">微信小程序自定义导航栏组件,navigation,完美适配全部手机</a>
  - <a href="https://github.com/kuckboy1994/mp_canvas_drawer" target="_blank">mp_canvas_drawer</a>微信小程序上canvas绘制图片助手，一个json就制作分享朋友圈图片
  - <a href="https://github.com/rchunping/wxapp-google-analytics" target="_blank">wxapp-google-analytics<a/> 让你的微信小程序支持谷歌统计(Google Analytics)

- 视频教程
  -  [极客学院微信小程序视频教程（全）](https://pan.baidu.com/s/1eSHXJG2 ) 链接: https://pan.baidu.com/s/1eSHXJG2 密码:ikwy
- ...
  - ...
  
> 希望大家贡献更多的微信小程序相关资源...

## 关于我
- Wechat : 742163033
- Website: [learnfans](https://www.learnfans.com)